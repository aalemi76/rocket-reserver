//
//  Coordinator.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit.UIViewController

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    init(navigationController: UINavigationController)
    
    func start()
    func push(_ viewController: UIViewController, animated: Bool)
    func present(_ viewController: UIViewController, animated: Bool)
    func dismiss(_ viewController: UIViewController, animated: Bool)
    func pop(animated: Bool)
}

extension Coordinator {
    
    func push(_ viewController: UIViewController, animated: Bool) {
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    func present(_ viewController: UIViewController, animated: Bool) {
        navigationController.present(viewController, animated: animated)
    }
    
    func dismiss(_ viewController: UIViewController, animated: Bool) {
        viewController.dismiss(animated: animated)
    }
    
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
}
