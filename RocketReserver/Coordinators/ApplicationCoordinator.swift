//
//  ApplicationCoordinator.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit.UIViewController

class ApplicationCoordinator {
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start(windowScene: UIWindowScene) {
        
    }
    
}
