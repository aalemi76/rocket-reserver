//
//  LaunchesViewModel.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Apollo
import Foundation

class LaunchesViewModel: ViewModelProvider {
    
    typealias model = LaunchListQuery
    
    let interactor: Interactor<LaunchListQuery>
    
    var view: Viewable?
    
    var launches = [LaunchListQuery.Data.Launch.Launch]()
    
    required init() {
        self.interactor = Interactor<LaunchListQuery>(model: LaunchListQuery())
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        getData()
    }
    
    func getData() {
        interactor.getModel { [weak self] graphQLResult in
            guard let self = self else { return }
            
            if let connection = graphQLResult.data?.launches {
                self.launches.append(contentsOf: connection.launches.compactMap({ $0 }))
            }
            print("### Launches list is: \(self.launches)")
            
            if let errors = graphQLResult.errors {
                let message = errors
                    .map { $0.localizedDescription }
                    .joined(separator: "\n")
                print(message)
                self.view?.show(result: .failure(errors.first!))
            }
        } onFailure: { [weak self] error in
            self?.view?.show(result: .failure(error))
        }

    }
}
