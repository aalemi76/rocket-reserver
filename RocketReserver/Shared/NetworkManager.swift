//
//  NetworkManager.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation
import Apollo

class NetworkManager {
    static let shared = NetworkManager()
    
    let domain = "https://apollo-fullstack-tutorial.herokuapp.com/graphql"
    
    //To Do: Make the unwrapping process safer!
    private(set) lazy var apollo = ApolloClient(url: URL(string: domain)!)
}
