//
//  Viewable.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

protocol Viewable {
    func show(result: Result<Any, Error>)
}
