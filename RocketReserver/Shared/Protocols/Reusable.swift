//
//  Reusable.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

protocol Reusable {
    // To initialize a reusable object
    init(reuseID: String, cellClass: AnyClass, model:Any)
    
    // To get reuse ID of the object
    func getReuseID() -> String
    
    // To retrieve the cell class of the object
    func getCellClass() -> AnyClass
    
    // To get the model of the class
    func getModel() -> Any
    
    // To do some updates when the corresponding cell is loaded
    func cellDidLoad(_ cell: Updatable)
}
