//
//  ViewModelProvider.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation
import Apollo

protocol ViewModelProvider: AnyObject {
    associatedtype model: GraphQLQuery
    init()
    func viewDidLoad(_ view: Viewable)
}
