//
//  Interactor.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation
import Apollo

class Interactor<Model: GraphQLQuery> {
    let model: Model
    
    init(model: Model) {
        self.model = model
    }
    
    func getModel(onSuccess: @escaping (GraphQLResult<Model.Data>) -> Void,
                  onFailure: @escaping (Error) -> Void) {
        
        DispatchQueue.global(qos: .background).async {
            NetworkManager.shared.apollo.fetch(query: self.model) { result in
                switch result {
                case .success(let graphQLResult):
                    onSuccess(graphQLResult)
                case .failure(let error):
                    onFailure(error)
                }
            }
        }
    }
}
