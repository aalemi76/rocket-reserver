//
//  TableViewContainer.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit

class TableViewContainer: UIView, TableViewProvider {
    var sections: [Sectionable]
    
    var tableView: UITableView
    
    var dataSourceHandler: TableViewDataSourceHandler
    
    var delegateHandler: TableViewDelegateHandler
    
    required init(tableView: UITableView) {
        self.sections = []
        
        self.tableView = tableView
        self.dataSourceHandler = TableViewDataSourceHandler(sections: sections)
        self.delegateHandler = TableViewDelegateHandler(sections: sections)
        super.init(frame: .zero)
        
        layoutTableView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layoutTableView() {
        addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo:topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    
}
