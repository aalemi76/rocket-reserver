//
//  TableCellViewModel.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

class TableCellViewModel: Reusable {
    let reuseID: String
    let cellClass: AnyClass
    let model: Any
    weak var cell: Updatable?
    
    required init(reuseID: String, cellClass: AnyClass, model: Any) {
        self.reuseID = reuseID
        self.cellClass = cellClass
        self.model = model
    }
    
    func getReuseID() -> String {
        return reuseID
    }
    
    func getCellClass() -> AnyClass {
        return cellClass
    }
    
    func getModel() -> Any {
        return model
    }
    
    func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
    }
    
    
}
