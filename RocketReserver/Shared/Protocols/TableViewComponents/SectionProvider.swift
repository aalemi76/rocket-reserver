//
//  SectionProvider.swift
//  RocketReserver
//
//  Created by AliReza on 2022-04-01.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

class SectionProvider: Sectionable {
    private var cells: [Reusable]
    
    required init(cells: [Reusable]) {
        self.cells = cells
    }
    
    func getCells() -> [Reusable] {
        return cells
    }
    
    func append(_ cells: [Reusable]) {
        self.cells += cells
    }
    
    
}
