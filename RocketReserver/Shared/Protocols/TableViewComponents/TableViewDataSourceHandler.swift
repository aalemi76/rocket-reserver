//
//  TableViewDataSourceHandler.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit.UITableView
import Combine

class TableViewDataSourceHandler: NSObject, UITableViewDataSource, UITableViewDataSourcePrefetching {
    var sections: [Sectionable]
    
    private(set) var loadNextPage = PassthroughSubject<IndexPath, Never>()
    
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numOfRows = sections[section].getCells().count
        return numOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionItems = sections[indexPath.section].getCells()
        let item = sectionItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.getReuseID(), for: indexPath)
        (cell as? Updatable)?.update(model: item.getModel())
        (cell as? Updatable)?.attach(viewModel: item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard let indexPath = indexPaths.last, indexPath.section < sections.count else { return }
        let lastSection = sections[indexPath.section]
        
        guard indexPath.row == lastSection.getCells().count - 1 else { return }
        loadNextPage.send(indexPath)
    }
    
}
