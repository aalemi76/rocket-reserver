//
//  TableViewDelegateHandler.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit.UITableView
import Combine

class TableViewDelegateHandler: NSObject, UITableViewDelegate {
    var sections: [Sectionable]
    private(set) var selectedItem = PassthroughSubject<Any, Never>()
    
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionItems = sections[indexPath.section].getCells()
        let item = sectionItems[indexPath.row].getModel()
        
        selectedItem.send(item)
    }
}
