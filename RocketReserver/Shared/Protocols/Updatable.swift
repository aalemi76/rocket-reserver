//
//  Updatable.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

protocol Updatable: AnyObject {
    
    // To assign model attribute to cell components
    func update(model: Any)
    
    // To attach view model of the cell in its class
    // in case it is required
    func attach(viewModel: Reusable)
}

extension Updatable {
    
    // Provide default implementation of the function
    func attach(viewModel: Reusable) {
        return
    }
}
