//
//  Sectionable.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import Foundation

protocol Sectionable {
    
    // To initialize the class
    init(cells: [Reusable])
    
    // To retrieve the cells of the section
    func getCells() -> [Reusable]
    
    // To append cells to the section
    func append(_ cells:[Reusable])
}
