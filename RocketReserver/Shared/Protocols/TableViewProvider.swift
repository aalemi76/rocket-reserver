//
//  TableViewProvider.swift
//  RocketReserver
//
//  Created by AliReza on 2022-03-30.
//  Copyright © 2022 Apollo GraphQL. All rights reserved.
//

import UIKit.UITableView

protocol TableViewProvider: AnyObject {
    var sections: [Sectionable] { get set }
    var tableView: UITableView { get }
    var dataSourceHandler: TableViewDataSourceHandler { get set }
    var delegateHandler: TableViewDelegateHandler { get set }
    
    init(tableView: UITableView)
    
    func registerSections()
    
    func load(_ sections: [Sectionable])
    
    func reloadSections(sections: IndexSet)
    
    func reloadItems(at indexPaths: [IndexPath])
    
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath)
    
}

extension TableViewProvider {
    
    func registerSections() {
        sections.forEach { (section) in
            section.getCells().forEach { reusable in
                let nibCell = UINib(nibName: reusable.getReuseID(), bundle: nil)
                tableView.register(nibCell, forCellReuseIdentifier: reusable.getReuseID())
            }
        }
    }
    
    func load(_ sections: [Sectionable]) {
        self.sections = sections
        
        dataSourceHandler = TableViewDataSourceHandler(sections: sections)
        delegateHandler = TableViewDelegateHandler(sections: sections)
        tableView.dataSource = dataSourceHandler
        tableView.prefetchDataSource = dataSourceHandler
        tableView.delegate = delegateHandler
        
        tableView.reloadData()
    }
    
    func reloadSections(sections: IndexSet) {
        tableView.reloadSections(sections, with: .automatic)
    }
    
    func reloadItems(at indexPaths: [IndexPath]) {
        tableView.reloadRows(at: indexPaths, with: .automatic)
    }
    
    func loadNextPage(_ items: [Reusable], from indexPath: IndexPath) {
        let lastSection = indexPath.section
        let lastRow = indexPath.row + 1
        let newIndexPaths = items.enumerated().map({ IndexPath(row: lastRow + $0.offset, section: lastSection)})
        sections[lastSection].append(items)
        tableView.insertRows(at: newIndexPaths, with: .automatic)
    }
}
